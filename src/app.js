"use strict";

// requirements
const express = require("express");

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get("/status", (req, res) => {
  res.status(200).end();
});
app.head("/status", (req, res) => {
  res.status(200).end();
});

// Main
app.get("/", (req, res) => {
  if (approval(req.query.amount)) {
    res.status(400).end(req.query.amount + " requires approval.");
  } else {
    res.status(200).end(req.query.amount + " does not require approval.");
  }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
const approval = (value) => {
  const MAX_CHAR = 10;
  const MAX = 1000000000;
  const threashold = 1000;
  const surcharge = 10;

  if (value === undefined || value === null || value === "") {
    throw RangeError("must have something");
  }

  if (typeof value !== "string" && typeof value !== "number") {
    throw RangeError("must be string or number");
  }

  if (value.length > MAX_CHAR) {
    throw RangeError("string exceeds max length " + MAX_CHAR);
  }

  if (!/^\+?(0|[1-9]\d*)$/.test(value)) {
    throw RangeError("string is not an interger");
  }

  const parsed = parseInt(value, 10);

  if (isNaN(parsed)) {
    throw RangeError("not a number");
  }

  if (parsed <= 0) {
    throw RangeError("amount must be positive");
  }
  if (parsed > MAX) {
    throw RangeError("amount cannot be more than " + MAX);
  }

  const amount = parsed + surcharge;
  if (amount >= threashold) {
    return true;
  }
  return false;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
  // HTTP listener
  app.listen(PORT, (err) => {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    console.log("Server is listening on port: ".concat(PORT));
  });
}
// CTRL+c to come to action
process.on("SIGINT", function () {
  process.exit();
});

module.exports = { app, approval };
