const request = require("supertest");
const { app, approval } = require("./app");

describe("security", () => {
  it("Request to 500, should return false", async () => {
    expect(approval("500")).toEqual(false);
  });

  it("Request to 1000, should return true", async () => {
    expect(approval("1000")).toEqual(true);
  });

  it("Request to 1000000000, should return true", async () => {
    expect(approval("1000000000")).toEqual(true);
  });

  it("Request to 1000000001, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(1000000001);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Request to -500, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(-500);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Int32bit maximum amount, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(2147483647);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result into Int32bit max value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(2147483647 - 10 + 1);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result into Int32bit max value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(-2147483648 - 10 - 1);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result into extreme large value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval("00000010323245498540985049580495849058043");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with hex value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with float value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval("10.2000");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with null value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(null);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with bad number string value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval("1234 ");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with bad number string value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval("1000000001");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it("Arithmetic result with Infinity value, should throw RangeError exception", async () => {
    expect.assertions(1);
    try {
      approval(Number.POSITIVE_INFINITY);
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });
});
